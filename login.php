<?php session_start(); ?>

<?php require "assets/inc/header.php" ?>

<?php

// Weiterleiten wenn bereits eingeloggt
if (isset($_SESSION['loggedin'])) {
    header("Location: dashboard.php");
    exit();
} else {

    // TODO: Wave Logo einfügen
    // FIXME: Hinweis wenn falsches Passwort eingegeben wurde
    // TODO: Loginversuche beschrenken 

    // Ausgabe Login Form
    echo '
    <main class="wave-login">
        <div class="full-screen">
            <div class="inner-div card">
                <div class="card-content wave-login-card">
                    <h4>Login</h4>
                    <form action="assets/exe/exe-login.php" method="POST">

                        <div class="input-field">
                            <input type="text" id="username" name="username" required>
                            <label for="username">Benutzername</label>
                        </div>

                        <div class="input-field">
                            <input type="password" id="password" name="password" required>
                            <label for="password">Passwort</label>
                        </div>

                        <button class="btn waves-effect waves-light" type="submit" name="action">Anmelden</button>
                    </form>                          
                </div>
            </div>
        </div>
    </main>';
}
?>

<?php require "assets/inc/footer.php" ?>