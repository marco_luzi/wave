<?php session_start();
if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

if (!isset($_GET['t'])) {
    header("Location: ../dashboard.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

?>

<?php require "assets/inc/header.php" ?>

<body>
    <main>

        <?php require "assets/inc/sidenav.php" ?>


        <div class="wave-content">
            <div class="container">

                <h5>Posts mit dem Tag: <?php echo $_GET['t'] ?></h5>

                <?php

                // Alle Posts mit Tag aus Datenbank auslesen.
                $post_array = ORM::for_table('posts')->join('posts_has_tags', array('posts.post_id', '=', 'posts_has_tags.posts_post_id'))->join('tags', array('posts_has_tags.tags_tag_id', '=', 'tags.tag_id'))->where('tags.tag_name', $_GET['t'])->find_many();

                // Loop für Posts
                foreach (array_reverse($post_array) as $post) {
                    $user = ORM::for_table('users')->where('user_id', $post['users_user_id'])->find_one();
                    $post_tags = ORM::for_table('posts_has_tags')->join('tags', array('posts_has_tags.tags_tag_id', '=', 'tags.tag_id'))->where('posts_has_tags.posts_post_id', $post['post_id'])->find_many();
                    $likes = ORM::for_table('likes')->where('users_user_id', $_SESSION['loggedin'])->where('posts_post_id', $post['post_id'])->find_one();

                    $likeclass = '';

                    // Like Button umfärben, wenn Post bereits geliked.
                    if ($likes == FALSE) {
                        $likeclass = 'like';
                    } else {
                        $likeclass = 'unlike blue-text';
                    };

                    echo '
                    <div class="card white black-text wave-post">
                    <div class="card-content">
                        <a href="user.php?u=' . $user['user_name'] . '"><img class="circle wave-post-img" src="' . getUsrImg($user['user_name']) . '"></a>
                        <p class="wave-post-usr"><a href="user.php?u=' . $user['user_name'] . '">@' . $user['user_name'] . '</a></p>
                        <p class="wave-post-txt">' . $post["post_msg"] . '</p>
                        <div class="wave-post-tags">';

                    foreach ($post_tags as $tags) {
                        echo '<a href="tag.php?t=' . $tags['tag_name'] . '"><div class="chip">' . $tags['tag_name'] . '</div></a>';
                    }


                    echo '</div>
                    </div>
                    <div class="card-action">
                        <div class="wave-post-since">
                            <p>' . timeElapsed($post['post_date']) . '</p>
                        </div>
                        <div class="wave-post-likes">
                            <div class="wave-likes">
                                <p><a href=""><i id="' . $post['post_id'] . '" class="far fa-thumbs-up ' . $likeclass . '"></i></a> ' . $post['post_likes'] . '</p>
                            </div>
                        </div>
                    </div>
                </div>
                ';
                }

                ?>

            </div>
        </div>


    </main>

    <?php require "assets/inc/footer.php" ?>