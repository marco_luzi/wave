<?php session_start();
if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

$post_user = ORM::for_table('users')->where('user_id', $_SESSION['loggedin'])->find_one();

?>

<?php require "assets/inc/header.php" ?>

<body>
    <main>

        <?php require "assets/inc/sidenav.php" ?>

        <div class="wave-content">
            <div class="container">

                <!-- Suchfeld für Ajax suche -->
                <form>
                    <div class="input-field search-box">
                        <i class="fas fa-search prefix"></i>
                        <input id="search-text" name="search-text" type="text" class="validate" style="color: black;">
                        <label for="search-text">Suche</label>
                    </div>
                </form>

                <div class="search-result"></div>

            </div>
        </div>

    </main>

    <?php require "assets/inc/footer.php" ?>