<?php session_start();
if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

?>

<?php require "assets/inc/header.php" ?>

<body>
    <main>

        <?php require "assets/inc/sidenav.php" ?>


        <div class="wave-content">
            <div class="container">

                <h3 class="center-align">Success!!</h3>
                <p class="center-align">Dein Post wurde erfolgreich erstellt.</p>
                <img class="center-img " src="assets/img/gifs/post-success.gif" alt="Tanzende Frau">

            </div>
        </div>

    </main>

    <?php require "assets/inc/footer.php" ?>