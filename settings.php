<?php session_start();
if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

?>

<?php require "assets/inc/header.php" ?>

<script>
    $(document).ready(function() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    });
                    $('.upload-demo').addClass('ready');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $uploadCrop = $('#upload-demo').croppie({
            viewport: {
                width: 270,
                height: 270,
                type: 'square'
            },
            boundary: {
                width: 450,
                height: 450
            }
        });

        $('#upload').on('change', function() {
            readFile(this);
        });
        $('.upload-result').on('click', function(ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'original'
            }).then(function(resp) {
                $('#imagebase64').val(resp);
                $('#form').submit();
            });
        });

    });
</script>

<body>
    <main>

        <?php require "assets/inc/sidenav.php" ?>

        <!-- TODO: Schöner gestalten -->
        <!-- TODO: Auf jpgs limitieren -->
        <!-- TODO: Ausgabe der Fehlermeldung -->

        <!-- Formular für Profilbildwechsel -->
        <div class="wave-content">
            <div class="container">

                <h3>Profilbild wechseln</h3>
                <p>Das Profilbild muss quadratisch und kleiner als 300KB sein.</p>
                <form action="assets/exe/exe-upload.php" method="post" enctype="multipart/form-data">
                    Bild auswählen:
                    <input type="file" name="fileToUpload" id="fileToUpload">
                    <input type="submit" class="btn" value=" Bild hochladen" name="submit">
                </form>

                <!-- <div class="row">
                    <div class="col s6">
                        <form action="assets/exe/exe-upload.php" id="form" method="post" enctype="multipart/form-data">
                            <input type="file" name="fileToUpload" id="upload" value="Choose a file">
                            <input type="hidden" id="imagebase64" name="imagebase64">
                        </form>
                    </div>

                    <div class="col s6">
                        <div id="upload-demo"></div>
                        <a href="#" class="upload-result">Send</a>
                    </div>
                </div> -->

            </div>
        </div>

    </main>

    <?php require "assets/inc/footer.php" ?>