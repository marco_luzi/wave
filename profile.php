<?php session_start();
if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

?>

<?php require "assets/inc/header.php" ?>

<body>
    <main>

        <?php require "assets/inc/sidenav.php" ?>

        <div class="wave-content">
            <div class="container">

                <!-- TODO: Follows und Follower markanter machen -->

                <!-- Infobereich des Profils -->

                <img class="circle wave-main-profile-img" src="<?php echo getUsrImg($loggedIn['user_name']); ?>">
                <p class="wave-profile-username">@<?php echo $loggedIn['user_name']; ?></p>
                <a href="follower.php?u=<?php echo $loggedIn['user_name'] ?>">
                    <p class="wave-profile-follower">Follower: <?php echo ORM::for_table('follows')->where('user_2', $_SESSION['loggedin'])->count(); ?></p>
                </a>
                <a href="follower.php?u=<?php echo $loggedIn['user_name'] ?>">
                    <p class="wave-profile-follows">Follows: <?php echo ORM::for_table('follows')->where('user_1', $_SESSION['loggedin'])->count(); ?></p>
                </a>

            </div>
            <div class="divider full-width"></div>
            <div class="container">

                <!-- TODO: Ajax Loading für viele Posts -->

                <?php

                // Persönliche Posts aus Datenbank holen
                $post_array = ORM::for_table('posts')->where('users_user_id', $_SESSION['loggedin'])->find_many();

                // Variable für einmalige ID für jeden Post
                $i = 0;

                //Loop für Posts
                foreach (array_reverse($post_array) as $post) {

                    $i += 1;

                    $post_tags = ORM::for_table('posts_has_tags')->join('tags', array('posts_has_tags.tags_tag_id', '=', 'tags.tag_id'))->where('posts_has_tags.posts_post_id', $post['post_id'])->find_many();
                    $post_prvt = $post['post_prvt'];
                    $likes = ORM::for_table('likes')->where('users_user_id', $_SESSION['loggedin'])->where('posts_post_id', $post['post_id'])->find_one();

                    $likeclass = '';

                    // Like Button umfärben, wenn Post bereits geliked.
                    if ($likes == FALSE) {
                        $likeclass = 'like';
                    } else {
                        $likeclass = 'unlike blue-text';
                    };

                    echo '
                    <div class="card white black-text wave-post">
                    <div class="card-content">
                        <div class="left">
                            <a href="user.php?u=' . $loggedIn['user_name'] . '"><img class="circle wave-post-img" src="' . getUsrImg($loggedIn['user_name']) . '"></a>
                            <p class="wave-post-usr"><a href="user.php?u=' . $loggedIn['user_name'] . '">@' . $loggedIn['user_name'] . '</a></p>
                        </div>
                        
                        <div class="dropdown-trigger right grey-text" data-target="dropdown' . $i . '"><i class="fas fa-ellipsis-v"></i></div>
                        <ul id="dropdown' . $i . '" class="dropdown-content">
                            <li><a href="assets/exe/exe-delete-post.php?p=' . $post["post_id"] . '"><i class="far fa-trash-alt"></i> Löschen</a></li>
                            <li class="divider" tabindex="-1"></li>';


                    // Dropdown anpassen, wenn Post privat oder öffentlich ist
                    if ($post_prvt == 1) {
                        echo '<li><a href="assets/exe/exe-publish-post.php?p=' . $post["post_id"] . '"><i class="fas fa-unlock"></i> Öffentlich</a></li>';
                    } else {
                        echo '<li><a href="assets/exe/exe-hide-post.php?p=' . $post["post_id"] . '"><i class="fas fa-lock"></i> Privat</a></li>';
                    }

                    echo '</ul>
                        <p class="wave-post-txt" style="margin-top: 60px !important;">' . $post["post_msg"] . '</p>
                        <div class="wave-post-tags">';

                    foreach ($post_tags as $tags) {
                        echo '<a href="tag.php?t=' . $tags['tag_name'] . '"><div class="chip">' . $tags['tag_name'] . '</div></a>';
                    }

                    echo '</div>
                    </div>
                    <div class="card-action">
                        <div class="wave-post-since">
                            <p>' . timeElapsed($post['post_date']) . '</p>
                        </div>
                        <div class="wave-post-likes">
                            <div class="wave-likes">
                                <p><a href=""><i id="' . $post['post_id'] . '" class="far fa-thumbs-up ' . $likeclass . '"></i></a> ' . $post['post_likes'] . '</p>
                            </div>
                        </div>
                    </div>
                </div>
                ';
                }

                ?>

            </div>
        </div>

    </main>

    <?php require "assets/inc/footer.php" ?>