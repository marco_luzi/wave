<?php require "assets/inc/header.php" ?>

<body style="overflow: hidden;">
    <main>
        <div class="row login-grid">
            <!-- Linke Seite der Startseite -->
            <div class="col s7 outer-div">
                <div class="inner-div">
                    <img src="assets/img/logo/logo-white.png" alt="Logo Wave">
                    <h4>WAVE | Die Beste Online Community</h4>
                    <p>WAVE ist die Social Media Plattform für jedermann, jedefrau und und sonstige Wesen. Entdecke eine Welt mit millionen von Nutzern, welche alle genau so viele Hobbys haben wie du ... nämlich keine.</p>
                </div>
            </div>

            <!-- Formular für Registrierung -->
            <div class="col s5 outer-div">
                <div class="inner-div card">
                    <div class="card-content">
                        <h4>Registrieren</h4>

                        <form action="assets/exe/exe-register.php" method="POST">

                            <div class="input-field">
                                <input type="email" id="email" name="email" required>
                                <label for="email">E-Mail</label>
                            </div>

                            <div class="input-field">
                                <input type="text" id="user" name="user" pattern="[A-Za-z0-9_-].{4,}" required>
                                <label for="user">Benutzername (min. 3 Zeichen inkl. - _)</label>
                            </div>

                            <div class="input-field">
                                <label for="password">Passwort (min. 8 Zeichen)</label>
                                <input type="password" class="check-password" id="password" name="password" pattern=".{6,}" required>
                                <meter max="4" id="password-strength-meter"></meter>
                                <p id="password-strength-text"></p>
                            </div>

                            <div class="input-field">
                                <input type="password" class="check-password" id="password-repeat" name="password-repeat" pattern=".{6,}" required>
                                <label for="password-repeat">Passwort wiederholen</label>
                            </div>

                            <button id="submit-button" class="btn waves-effect waves-light" type="submit" name="submit" disabled>Registrieren</button>
                        </form>
                        <a href="login.php"><button class="btn wave-login-btn">Oder Anmelden</button></a>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php require "assets/inc/footer.php" ?>