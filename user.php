<?php session_start();

if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

$user = ORM::for_table('users')->where('user_name', $_GET['u'])->find_one();
$loggedIn = ORM::for_table('users')->where('user_id', $_SESSION['loggedin'])->find_one();

// Weiterletung wenn man auf eigenen User klickt.
if ($_GET['u'] === $loggedIn['user_name']) {
    header("Location: ../profile.php");
    exit();
}

?>

<?php require "assets/inc/header.php" ?>

<body>
    <main>

        <?php require "assets/inc/sidenav.php" ?>

        <div class="wave-content">
            <div class="container">

                <!-- TODO: Follows und Follower markanter machen -->

                <!-- Infobereich des Profils -->

                <img class="circle wave-main-profile-img" src="<?php echo getUsrImg($user['user_name']); ?>">
                <p class="wave-profile-username">@<?php echo $user['user_name']; ?></p>
                <a href="follower.php?u=<?php echo $_GET['u'] ?>">
                    <p class="wave-profile-follower">Follower: <?php echo ORM::for_table('follows')->where('user_2', $user['user_id'])->count(); ?></p>
                </a>
                <a href="follower.php?u=<?php echo $_GET['u'] ?>">
                    <p class="wave-profile-follows">Follows: <?php echo ORM::for_table('follows')->where('user_1', $user['user_id'])->count(); ?></p>
                </a>

                <!-- TODO: Ajax Loading für viele Posts -->

                <?php

                // Überprüfen, ob bereits gefollowt
                $follows = ORM::for_table('follows')->where('user_1', $_SESSION['loggedin'])->where('user_2', $user['user_id'])->find_one();

                if ($follows != NULL) {
                    echo '<form action="assets/exe/exe-unfollow.php">
                    <button class="btn waves-effect waves-light full-width red" type="submit" value="' . $_GET['u'] . '" name="unfollow" style="margin-bottom: 20px;">Nicht mehr folgen</button>
                </form>';
                } else {
                    echo '<form action="assets/exe/exe-follow.php">
                    <button class="btn waves-effect waves-light full-width" type="submit" value="' . $_GET['u'] . '" name="follow" style="margin-bottom: 20px;">Folgen</button>
                </form>';
                }
                ?>


            </div>
            <div class="divider full-width"></div>
            <div class="container">

                <?php

                // Posts aus Datenbank holen
                $post_array = ORM::for_table('posts')->where(array('users_user_id' => $user['user_id'], 'post_prvt' => 0))->find_many();

                // Posts darstellen
                foreach (array_reverse($post_array) as $post) {
                    $post_tags = ORM::for_table('posts_has_tags')->join('tags', array('posts_has_tags.tags_tag_id', '=', 'tags.tag_id'))->where('posts_has_tags.posts_post_id', $post['post_id'])->find_many();
                    $likes = ORM::for_table('likes')->where('users_user_id', $_SESSION['loggedin'])->where('posts_post_id', $post['post_id'])->find_one();

                    $likeclass = '';

                    // Like Button umfärben, wenn Post bereits geliked.
                    if ($likes == FALSE) {
                        $likeclass = 'like';
                    } else {
                        $likeclass = 'unlike blue-text';
                    };

                    echo '
                    <div class="card white black-text wave-post">
                    <div class="card-content">
                        <a href="#!"><img class="circle wave-post-img" src="' . getUsrImg($user['user_name']) . '"></a>
                        <p class="wave-post-usr"><a href="#!">@' . $user['user_name'] . '</a></p>
                        <p class="wave-post-txt">' . $post["post_msg"] . '</p>
                        <div class="wave-post-tags">';

                    foreach ($post_tags as $tags) {
                        echo '<a href="tag.php?t=' . $tags['tag_name'] . '"><div class="chip">' . $tags['tag_name'] . '</div></a>';
                    }

                    echo '</div>
                    </div>
                    <div class="card-action">
                        <div class="wave-post-since">
                            <p>' . timeElapsed($post['post_date']) . '</p>
                        </div>
                        <div class="wave-post-likes">
                            <div class="wave-likes">
                            <p><a href=""><i id="' . $post['post_id'] . '" class="far fa-thumbs-up ' . $likeclass . '"></i></a> ' . $post['post_likes'] . '</p>
                            </div>
                        </div>
                    </div>
                </div>
                ';
                }

                ?>

            </div>
        </div>

    </main>

    <?php require "assets/inc/footer.php" ?>