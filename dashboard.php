<?php session_start();
if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

?>

<?php require "assets/inc/header.php" ?>

<body>
    <main>

        <!-- Hidden overlay für neuen Post -->
        <div class="wave-new-post-overlay full-screen">
            <div class="card white black-text wave-new-post-card inner-div">
                <div class="card-content">
                    <a href="" class="wave-close-button" id="close-new-post"><i class="fas fa-times"></i></a>
                    <h4>Neuer Post erstellen</h4>
                    <form action="assets/exe/exe-create-post.php" method="POST">
                        <div class="input-field">
                            <textarea id="wave-message" name="wave-message" class="materialize-textarea" data-length="280"></textarea>
                            <label for="wave-message">Nachricht</label>
                        </div>

                        <!-- TODO: Keine leeren Tags -->

                        <div class="input-field">
                            <input id="wave-tags" name="wave-tags" type="text" data-length="10">
                            <label for="wave-tags">Tags (mit Leerzeichen trennen)</label>
                        </div>
                        <p>
                            <label>
                                <input type="checkbox" id="prvt-box" class="filled-in" name="prvt-box" value="1" />
                                <span>Privat</span>
                            </label>
                        </p>
                        <input type="hidden" id="prvt-box-hidden" name="prvt-box-hidden" value="0" />
                        <button class="btn waves-effect waves-light" type="submit" name="submit">Post erstellen</button>
                    </form>
                </div>
            </div>
        </div>

        <?php require "assets/inc/sidenav.php" ?>


        <div class="wave-content">
            <div class="container">

                <!-- Button Neuer Post -->
                <!-- TODO: Neuer Post Button verschönern und umpositionieren -->

                <div class="wave-new-post">
                    <a class="waves-effect waves-light btn full-width" id="new-post">Neuer Post erstellen</a>
                </div>

                <!-- TODO: Ajax Loading für viele Posts -->

                <?php

                // Prepare Query für Posts im Dashboard
                $query = "SELECT * FROM ((SELECT * FROM posts WHERE posts.users_user_id = " . $_SESSION['loggedin'] . ") UNION (SELECT posts.post_id, posts.post_date, posts.post_msg, posts.post_prvt, posts.post_likes, posts.users_user_id FROM posts JOIN follows ON posts.users_user_id = follows.user_2 WHERE follows.user_1 =  " . $_SESSION['loggedin'] . ")) posts WHERE post_prvt != 1 ORDER BY posts.post_date DESC;";
                $post_array = ORM::for_table('posts')->raw_query($query)->find_many();

                // Loop für Posts
                foreach ($post_array as $post) {
                    $user = ORM::for_table('users')->where('user_id', $post['users_user_id'])->find_one();
                    $post_tags = ORM::for_table('posts_has_tags')->join('tags', array('posts_has_tags.tags_tag_id', '=', 'tags.tag_id'))->where('posts_has_tags.posts_post_id', $post['post_id'])->find_many();
                    $likes = ORM::for_table('likes')->where('users_user_id', $_SESSION['loggedin'])->where('posts_post_id', $post['post_id'])->find_one();

                    $likeclass = '';

                    // Like Button umfärben, wenn Post bereits geliked.
                    if ($likes == FALSE) {
                        $likeclass = 'like';
                    } else {
                        $likeclass = 'unlike blue-text';
                    };

                    // Ausgabe Post
                    echo '
                    <div class="card white black-text wave-post">
                    <div class="card-content">
                        <a href="user.php?u=' . $user['user_name'] . '"><img class="circle wave-post-img" src="' . getUsrImg($user['user_name']) . '"></a>
                        <p class="wave-post-usr"><a href="user.php?u=' . $user['user_name'] . '">@' . $user['user_name'] . '</a></p>
                        <p class="wave-post-txt">' . $post["post_msg"] . '</p>
                        <div class="wave-post-tags">';

                    // Loop für Tags
                    foreach ($post_tags as $tags) {
                        echo '<a href="tag.php?t=' . $tags['tag_name'] . '"><div class="chip">' . $tags['tag_name'] . '</div></a>';
                    }

                    echo '</div>
                    </div>
                    <div class="card-action">
                        <div class="wave-post-since">
                            <p>' . timeElapsed($post['post_date']) . '</p>
                        </div>
                        <div class="wave-post-likes">
                            <div class="wave-likes">
                                <p><a href=""><i id="' . $post['post_id'] . '" class="far fa-thumbs-up ' . $likeclass . '"></i></a> ' . $post['post_likes'] . '</p>
                            </div>
                        </div>
                    </div>
                </div>';
                }
                ?>

            </div>
        </div>

    </main>

    <?php require "assets/inc/footer.php" ?>