<?php session_start();
if (!isset($_SESSION['loggedin'])) {
    header("Location: ../login.php");
    exit();
}

include 'assets/inc/idiorm.php';
include 'assets/inc/elapsed_time.php';
include 'assets/inc/profile_img.php';

?>

<?php require "assets/inc/header.php" ?>

<body>
    <main>

        <?php require "assets/inc/sidenav.php" ?>

        <div class="wave-content">
            <div class="container">

                <div class="row">
                    <!-- Liste mit Follows -->
                    <div class="col s12 m6">
                        <h5>Follows</h5>

                        <?php

                        // Infos zu Follows aus Datebank auslesen
                        $user_id = ORM::for_table('users')->where('user_name', $_GET['u'])->find_one();
                        $follows = ORM::for_table('follows')->join('users', array('users.user_id', '=', 'follows.user_2'))->where('follows.user_1', $user_id['user_id'])->find_many();

                        //Loop für Follows
                        foreach ($follows as $follow) {
                            echo '
                                <div class="card white black-text wave-post">
                                    <div class="card-content">
                                        <a href="user.php?u=' . $follow['user_name'] . '"><img class="circle wave-post-img" src="' . getUsrImg($follow['user_name']) . '"></a>
                                        <p class="wave-post-usr"><a href="user.php?u=' . $follow['user_name'] . '">@' . $follow['user_name'] . '</a></p>
                                    </div>
                                </div>';
                        } ?>
                    </div>

                    <!-- Liste mit Follower -->
                    <div class="col s12 m6">
                        <h5>Follower</h5>

                        <?php

                        // Infos zu Follower aus Datebank auslesen
                        $follower = ORM::for_table('users')->join('follows', array('users.user_id', '=', 'follows.user_1'))->where('follows.user_2', $user_id['user_id'])->find_many();

                        //Loop für Followers
                        foreach ($follower as $follow) {
                            echo '
                                <div class="card white black-text wave-post">
                                    <div class="card-content">
                                        <a href="user.php?u=' . $follow['user_name'] . '"><img class="circle wave-post-img" src="' . getUsrImg($follow['user_name']) . '"></a>
                                        <p class="wave-post-usr"><a href="user.php?u=' . $follow['user_name'] . '">@' . $follow['user_name'] . '</a></p>
                                    </div>
                                </div>';
                        } ?>

                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php require "assets/inc/footer.php" ?>