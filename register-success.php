<?php require "assets/inc/header.php" ?>

<body>
    <main class="wave-login">
        <div class="full-screen">
            <div class="inner-div card">
                <div class="card-content wave-login-card">
                    <h4>E-Mail Adresse bestätigen</h4>
                    <p>Wir haben dir eine E-Mail zur Überprüfung zugesendet. Bitte überprüfe dein Postfach um die Registrierung abzuschliessen.</p>                    
                </div>
            </div>
        </div>
    </main>


<?php require "assets/inc/footer.php" ?>