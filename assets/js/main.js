$(document).ready(function() {
  //Materialize Tools initiiren
  $(".sidenav").sidenav();

  $(".tooltipped").tooltip();

  $("textarea#wave-message").characterCounter();

  $(".dropdown-trigger").dropdown({
    alignment: "right",
    constrainWidth: false,
    coverTrigger: false
  });

  //Neuer Post Overlay anzeigen
  $("#new-post").click(function() {
    $(".wave-new-post-overlay").css("display", "block");
  });

  //Neuer Post Overlay entfernen
  $("#close-new-post").click(function() {
    $(".wave-new-post-overlay").css("display", "none");
  });

  //Ajax für Post suche
  load_data();
  function load_data(query) {
    $.ajax({
      url: "assets/exe/exe-fetch.php",
      method: "post",
      data: { query: query },
      success: function(data) {
        $(".search-result").html(data);
      }
    });
  }

  //Automatisches suchen bei Keyup
  $("#search-text").keyup(function() {
    var search = $(this).val();
    if (search != "") {
      load_data(search);
    } else {
      load_data();
    }
  });

  //Ajax fürs liken
  $(".like").click(function() {
    var post_id = $(this).attr("id");
    $.ajax({
      url: "assets/exe/exe-like.php",
      type: "post",
      async: false,
      data: {
        liked: 1,
        post_id: post_id
      },
      success: function() {}
    });
  });

  //Ajax fürs unliken
  $(".unlike").click(function() {
    var post_id = $(this).attr("id");
    $.ajax({
      url: "assets/exe/exe-unlike.php",
      type: "post",
      async: false,
      data: {
        unliked: 1,
        post_id: post_id
      },
      success: function() {}
    });
  });

  //Register Button deaktivieren, wenn Passwörter nicht übereinstimmen
  $(".check-password").on("keyup", function() {
    if ($("#password").val() == $("#password-repeat").val()) {
      $("#submit-button").prop("disabled", false);
    } else {
      $("#submit-button").prop("disabled", true);
    }
  });

  //Passwort Strength Meter
  var strength = {
    0: "Worst",
    1: "Bad",
    2: "Weak",
    3: "Good",
    4: "Strong"
  };

  var password = document.getElementById("password");
  var meter = document.getElementById("password-strength-meter");
  var text = document.getElementById("password-strength-text");

  password.addEventListener("input", function() {
    var val = password.value;
    var result = zxcvbn(val);

    // Update the password strength meter
    meter.value = result.score;

    console.log(result.score);

    // Update the text indicator
    if (val !== "") {
      text.innerHTML = "Strength: " + strength[result.score];
    } else {
      text.innerHTML = "";
    }
  });
});
