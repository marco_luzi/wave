<?php session_start();

if (!empty($_GET)) {

    include '../inc/idiorm.php';

    $user = ORM::for_table('users')->where('user_name', $_GET['unfollow'])->find_one();
    $currentUser = $_SESSION['loggedin'];

    //Datenbank eintrag löschen
    $unfollows = ORM::for_table('follows')->where('user_1', $currentUser)->where('user_2', $user['user_id'])->find_one();
    $unfollows->delete();

    header("Location: ../../user.php?u=" . $_GET['unfollow']);
    exit();
}
