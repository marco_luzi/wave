<?php

if (isset($_GET["token"])) {
    $token = $_GET["token"];
}

include '../inc/idiorm.php';

$verify = ORM::for_table('verify')->where('verify_token', $token)->find_one();
$user = ORM::for_table('users')->create();

if (!$verify) {
    echo "<script type='text/javascript'>alert('Token abgelaufen. Bitte registriere dich erneut.'); window.location.href='../../index.php';</script>";
    exit();
}

//User Tabelle mit Daten füllen
$user->user_name = $verify->verify_name;
$user->user_email = $verify->verify_email;
$user->user_hash = $verify->verify_hash;
$user->user_salt = $verify->verify_salt;
$user->save();

//Verify Eintrag löschen
$verify->delete();

header('Location: ../../login.php');
exit;
