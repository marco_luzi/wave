<?php

session_start();


if (!isset($_SESSION['loggedin'])) {

    include '../inc/idiorm.php';

    $users = ORM::for_table('users')->where('user_name', $_POST['username'])->find_one();

    //Abrufen des Salt und Hashs
    $hash_db = $users->user_hash;
    $salt_db = $users->user_salt;

    //Eingegebenes Passwort mit Salt hasen
    $hash = hash_pbkdf2("sha256", $_POST['password'], $salt_db, 1000, 64);

    //Hashwerte vergleichen und $_SESSION mit user_id befüllen
    if ($hash == $hash_db) {
        $_SESSION['loggedin'] = $users->user_id;
        header("Location: ../../dashboard.php");
        exit();
    } else {
        header("Location: ../../login.php#wrong-pw");
    }
} else {
    header("Location: ../../dashboard.php");
}
