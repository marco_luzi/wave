<?php session_start();

include '../inc/idiorm.php';
include '../inc/elapsed_time.php';
include '../inc/profile_img.php';

//Datenbank verbindung ohne Idiorm
$connect = mysqli_connect("localhost", "luzimarc_wave", "]d2FGwDr!G2Uu%}", "luzimarc_wave");
$output = '';
if (isset($_POST["query"])) {
    $search = mysqli_real_escape_string($connect, $_POST["query"]);

    //Query für Suche
    $query = "
    SELECT * FROM posts 
    INNER JOIN users ON posts.users_user_id = users.user_id
    WHERE post_msg LIKE '%" . $search . "%'
    AND post_prvt = 0
	";
} else {
    exit();
}
$result = mysqli_query($connect, $query);
if (mysqli_num_rows($result) > 0) {

    //Output Loop für Suchergebnis
    while ($row = mysqli_fetch_array($result)) {

        $post_tags = ORM::for_table('posts_has_tags')->join('tags', array('posts_has_tags.tags_tag_id', '=', 'tags.tag_id'))->where('posts_has_tags.posts_post_id', $row['post_id'])->find_many();
        $likes = ORM::for_table('likes')->where('users_user_id', $_SESSION['loggedin'])->where('posts_post_id', $row['post_id'])->find_one();

        $likeclass = '';

        if ($likes == FALSE) {
            $likeclass = 'like';
        } else {
            $likeclass = 'unlike blue-text';
        };

        $output .= '
        <div class="card white black-text wave-post">
            <div class="card-content">
                <a href="user.php?u=';

        $output .= $row['user_name'];

        $output .= '
                "><img class="circle wave-post-img" src="';

        $output .= getUsrImg($row['user_name']);

        $output .= '
                "></a>
                <p class="wave-post-usr"><a href="user.php?u=';

        $output .= $row['user_name'];

        $output .= '
                ">@';

        $output .= $row['user_name'];

        $output .= '
                </a></p>
                <p class="wave-post-txt">';

        $output .= $row['post_msg'];

        $output .= '
                </p>
                <div class="wave-post-tags">';

        foreach ($post_tags as $tags) {
            $output .= '<a href=""><div class="chip">' . $tags['tag_name'] . '</div></a>';
        };

        $output .= '
                </div>
            </div>
            <div class="card-action">
                <div class="wave-post-since">
                    <p>';

        $output .= timeElapsed($row['post_date']);

        $output .= '
                    </p>
                </div>
                <div class="wave-post-likes">
                    <div class="wave-likes">
                    <p><i style="margin-right: 8px;" id="';

        $output .= $row['post_id'];

        $output .= '
                    " class="far fa-thumbs-up ';

        $output .= $likeclass;

        $output .= '
                    "></i>';

        $output .= $row['post_likes'];

        $output .= '
                        </p>
                    </div>
                </div>
            </div>
        </div>
        ';
    }
    echo $output;
} else {
    echo 'Keine Posts gefunden. So sad...';
}
