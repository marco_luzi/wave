<?php session_start();

if (!empty($_POST)) {

    include '../inc/idiorm.php';

    $username = ORM::for_table('users')->where('user_id', $_SESSION['loggedin'])->find_one();

    //Daten für Upload angeben
    $target_dir = "../img/profile/";

    // Überprüfen, ob datei ein Bild ist.
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["upload"]["tmp_name"]);
        if ($check !== false) {
            echo "Datei ist ein Bild - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "Datei ist kein Bild.";
            $uploadOk = 0;
        }
    }

    // Bildgrösse überprüfen
    if ($_FILES["upload"]["size"] > 500000) {
        echo "Das Bild ist zu gross.";
        $uploadOk = 0;
    }



    $target_file = $target_dir . basename($_FILES["upload"]["name"]);
    $name = $username['user_name'];
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    echo $_FILES["upload"]["name"];
    echo $target_file;
    if (isset($_FILES["imagebase64"])) {
        echo "isset";
    } else {
        echo "not set";
    }





    // Datentyp überprüfen
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    ) {
        echo "Nur JPG, JPEG & PNG Bilder sind erlaubt.";
        $uploadOk = 0;
    }

    // Überprüfen, ob es beim upload einen fehler gegeben hat
    if ($uploadOk == 0) {
        //echo "<script type='text/javascript'>alert('Das Bild wurde nicht hochgeladen.'); window.location.href='../../settings.php';</script>";
        // Wenn keine Fehler -> Bild hochladen
    } else {
        if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_dir . $name . "." . $imageFileType)) {
            clearstatcache();
            //echo "<script type='text/javascript'>alert('Die Datei " . basename($_FILES["upload"]["name"]) . " wurde hochgeladen.'); window.location.href='../../settings.php';</script>";
            exit();
        } else {
            //echo "<script type='text/javascript'>alert('Es gab einen Fehler beim Hochladen. Versuche es erneut.'); window.location.href='../../settings.php';</script>";
        }
    }

    clearstatcache();

    //echo "<script type='text/javascript'>alert('Das Bild wurde nicht hochgeladen.'); window.location.href='../../settings.php';</script>";
    exit();
}
