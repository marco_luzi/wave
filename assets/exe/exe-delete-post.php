<?php session_start();

include '../inc/idiorm.php';

$post_id = $_GET['p'];

$post = ORM::for_table('posts')->where('post_id', $post_id)->find_one();

if ($post['users_user_id'] = $_SESSION['loggedin']) {

    $posts_has_tags = ORM::for_table('posts_has_tags')->where('posts_post_id', $post_id)->delete_many();
    $likes = ORM::for_table('likes')->where('posts_post_id', $post_id)->delete_many();

    $post->delete();

    header("Location: ../../profile.php");
    exit();
}


header("Location: ../../profile.php");
exit();
