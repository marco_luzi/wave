<?php session_start();

include '../inc/idiorm.php';

$post_id = $_GET['p'];

$post = ORM::for_table('posts')->where('post_id', $post_id)->find_one();

if ($post['users_user_id'] = $_SESSION['loggedin']) {
    $post->post_prvt = 0;
    $post->save();
}

header("Location: ../../profile.php");
exit();
