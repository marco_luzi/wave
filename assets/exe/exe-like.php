<?php session_start();

include '../inc/idiorm.php';
include '../inc/elapsed_time.php';
include '../inc/profile_img.php';

if (isset($_POST['liked'])) {
    $postid = $_POST['post_id'];

    //Anzahl Likes um eins erhöhen
    $post = ORM::for_table('posts')->where('post_id', $postid)->find_one();
    $post->post_likes += 1;
    $post->save();

    //Like Realtion in likes Tabelle eintragen
    $likes = ORM::for_table('likes')->create();
    $likes->users_user_id = $_SESSION['loggedin'];
    $likes->posts_post_id = $postid;
    $likes->save();
}
