<?php session_start();

include '../inc/idiorm.php';
include '../inc/elapsed_time.php';
include '../inc/profile_img.php';

if (isset($_POST['unliked'])) {
    $postid = $_POST['post_id'];

    //Like anzahl um eins reduzieren
    $post = ORM::for_table('posts')->where('post_id', $postid)->find_one();
    $post->post_likes -= 1;
    $post->save();

    //Relation in Tabelle likes löschen
    $likes = ORM::for_table('likes')->where(array(
        'users_user_id' => $_SESSION['loggedin'],
        'posts_post_id' => $postid
    ))->find_one();

    $likes->delete();
}
