<?php

if (!empty($_POST)) {

    include '../inc/idiorm.php';

    $password = $_POST['password'];
    $password_repeat = $_POST['password-repeat'];

    if ($password != $password_repeat) {
        echo "<script type='text/javascript'>alert('Passwörter stimmen nicht überein. Versuche es erneut.'); window.location.href='../../index.php';</script>";
        exit();
    }

    //Salt erstellen und mit eingegebenem Passwort hashen
    $salt = bin2hex(openssl_random_pseudo_bytes(64));
    $hash = hash_pbkdf2("sha256", $password, $salt, 1000, 64);

    $date = date("Y-m-d H:i:s");

    //Token für verifikation erstellen
    $token = openssl_random_pseudo_bytes(16);
    $token = bin2hex($token);

    //Daten in Tabelle Verify einfüllen
    $verify = ORM::for_table('verify')->create();
    $verify->verify_name = $_POST['user'];
    $verify->verify_email = $_POST['email'];
    $verify->verify_hash = $hash;
    $verify->verify_salt = $salt;
    $verify->verify_date = $date;
    $verify->verify_token = $token;
    $verify->save();

    //E-Mail versenden
    include '../inc/email-verification-send.php';

    header("Location: ../../register-success.php");
    exit;
}
