<?php session_start();

if (!empty($_POST)) {

    include '../inc/idiorm.php';

    // Daten aus dem Formular erhalten.
    $message = $_POST['wave-message'];
    $post_tag_array = $_POST['wave-tags'];
    $prvt = 0;

    if ($_POST['prvt-box'] === NULL) {
        $prvt = 0;
    } else if ($_POST['prvt-box'] == 1) {
        $prvt = 1;
    }

    // Tag string in lowercase umwandeln und array erstellen.
    $post_tag_array = strtolower($post_tag_array);
    $post_tag_array = explode(" ", $post_tag_array);

    date_default_timezone_set('UTC');
    $date = date("Y-m-d H:i:s");

    $user_id = $_SESSION['loggedin'];

    // Daten in posts Tabelle schreiben.
    $post = ORM::for_table('posts')->create();
    $post->post_date = $date;
    $post->post_msg = $message;
    $post->post_prvt = $prvt;
    $post->users_user_id = $user_id;
    $post->save();

    // Array mit allen Tags aus der Datenbank holen.
    $tags_exists = ORM::for_table('tags')->find_array();

    // Function um heruauszufinden, ob Tag bereits in der Datenbank ist.
    function checkIfTagInArray($tag, $array, $strict = false)
    {
        // Foreach Loop für die einzelnen Elemente auf der Datenbak.
        foreach ($array as $item) {
            // Überprüft, ob Tag mit in Datenbank vorkommt. Überprüft auch wenn der Array multidimensional ist.
            if (($strict ? $item === $tag : $item == $tag) || (is_array($item) && checkIfTagInArray($tag, $item, $strict))) {
                return true;
            }
        }
        return false;
    }
    function getTagId(string $post_tag)
    {
        //$tagId  = ORM::for_table('tags')->raw_query('SELECT tag_id FROM tags WHERE tag_name = ' . $post_tag . ';')->find_many();
        $tagId  = ORM::for_table('tags')->where('tag_name', $post_tag)->find_one();
        return (string) $tagId["tag_id"];
    }
    function getPostId(string $date, int $user_id)
    {
        //$postId = ORM::for_table('posts')->raw_query('SELECT post_id FROM posts WHERE post_date = ' . $date . ' AND users_user_id = ' . $user_id . ';')->find_many();
        $postId  = ORM::for_table('posts')->where(array(
            'post_date' => $date,
            'users_user_id' => $user_id
        ))->find_one();
        return (string) $postId["post_id"];
    }

    $tag_count = 0;

    foreach ($post_tag_array as $post_tag) {

        $tag_count += 1;

        if ($tag_count > 4) {
            header("Location: ../../dashboard.php");
            exit();
        }

        // Ausführen der Funktion checkIfTagInArray und schreibt Datensatz in die Tabelle wenn vorhanden.
        if (!checkIfTagInArray($post_tag, $tags_exists)) {

            $create_tag = ORM::for_table('tags')->create();
            $create_tag->tag_name = $post_tag;
            $create_tag->save();

            $tagId = getTagId($post_tag);
            $postId = getPostId($date, $user_id);

            $create_relation = ORM::for_table('posts_has_tags')->create();
            $create_relation->posts_post_id = $postId;
            $create_relation->tags_tag_id = $tagId;
            $create_relation->save();
        } else {

            // Erstelle n zu m Relationen in der Tabelle posts_has_tags.
            $tagId = getTagId($post_tag);
            $postId = getPostId($date, $user_id);

            $create_relation = ORM::for_table('posts_has_tags')->create();
            $create_relation->posts_post_id = $postId;
            $create_relation->tags_tag_id = $tagId;
            $create_relation->save();
        }
    };

    header("Location: ../../dashboard.php");
    exit();
}

header("Location: ../../dashboard.php");
exit();
