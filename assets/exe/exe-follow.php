<?php session_start();

if (!empty($_GET)) {

    include '../inc/idiorm.php';

    $user = ORM::for_table('users')->where('user_name', $_GET['follow'])->find_one();
    $currentUser = $_SESSION['loggedin'];

    //Follow Tabelle befüllen
    $follows = ORM::for_table('follows')->create();
    $follows->user_1 = $currentUser;
    $follows->user_2 = $user['user_id'];
    $follows->save();

    header("Location: ../../user.php?u=" . $_GET['follow']);
    exit();
}
