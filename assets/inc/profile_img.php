<?php session_start();

function getUsrImg($userName)
{
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/assets/img/profile/' . $userName . '.jpg')) {
        return 'assets/img/profile/' . $userName . '.jpg';
    } else {
        return 'assets/img/profile/wave_default.jpg';
    }
}
