<?php

$loggedIn = ORM::for_table('users')->where('user_id', $_SESSION['loggedin'])->find_one();

echo '
    <ul id="slide-out" class="sidenav sidenav-fixed wave-sidenav">
        <li><div class="user-view wave-profile-img">
            <a href="profile.php"><img class="circle" src="' . getUsrImg($loggedIn['user_name']) . '"></a>
            <p class="wave-username"><a href="profile.php">@' . $loggedIn['user_name'] . '</a></p>
        </div></li>
        <li><div class="divider"></div></li>
        <li><a class="waves-effect" href="dashboard.php"><i class="fab fa-bandcamp"></i>Dashboard</a></li>
        <li><a class="waves-effect" href="profile.php"><i class="fas fa-address-card"></i>Profil</a></li>
        <li><a class="waves-effect" href="settings.php"><i class="fas fa-cog"></i>Einstellungen</a></li>
        <li><a class="waves-effect" href="search.php"><i class="fas fa-search"></i>Suchen</a></li>
        <li><a class="waves-effect" href="assets/exe/exe-logout.php"><i class="fas fa-sign-out-alt"></i>Abmelden</a></li>
    </ul>
    ';
