<?php

function timeElapsed($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed;
    $minutes    = round($time_elapsed / 60);
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400);
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640);
    $years      = round($time_elapsed / 31207680);
    // Sekunden
    if ($seconds <= 60) {
        return "Jetzt";
    }
    //Minuten
    else if ($minutes <= 60) {
        if ($minutes == 1) {
            return "Vor einer Minute";
        } else {
            return "Vor $minutes Minuten";
        }
    }
    //Stunden
    else if ($hours <= 24) {
        if ($hours == 1) {
            return "Vor einer Stunde";
        } else {
            return "Vor $hours Stunden";
        }
    }
    //Tage
    else if ($days <= 7) {
        if ($days == 1) {
            return "Gestern";
        } else {
            return "Vor $days Tagen";
        }
    }
    //Wochen
    else if ($weeks <= 4.3) {
        if ($weeks == 1) {
            return "Vor einer Woche";
        } else {
            return "Vor $weeks Wochen";
        }
    }
    //Monate
    else if ($months <= 12) {
        if ($months == 1) {
            return "Vor einem Monat";
        } else {
            return "Vor $months Monaten";
        }
    }
    //Jahre
    else {
        if ($years == 1) {
            return "Vor einem Jahr";
        } else {
            return "Vor $years Jahren";
        }
    }
}
